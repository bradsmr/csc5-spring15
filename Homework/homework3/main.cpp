/*
* Name: Bradley Summers
* Student ID: 2564199
* Date: due 3/19/15
* HW: homework3
* Problem: 
* I certify this is my own work and code
*/

#include <iostream>

using namespace std;

int main()
{
    
    // 1) PHONE NUMBER WITHOUT DASH
    
        // get phone number input
    string s_phonenumber;
    cout << "Enter a phone number with only one dash." << endl
         << "> ";
    cin  >> s_phonenumber;   
    
        // create two strings without the dash
    int    i_dashloc    = s_phonenumber.find("-");
    string s_firsthalf  = s_phonenumber.substr(0,i_dashloc);
    string s_secondhalf = s_phonenumber.substr(i_dashloc + 1);
    
        // output the two strings
    cout << s_firsthalf << s_secondhalf;
    
    // 2)       
    return 0;
}