/*
* Name: Bradley Summers
* Student ID: 2564199
* Date: 2/23/15
* HW: homework0
* Problem: Output my favorite food & name.
* I certify this is my own work and code
*/

#include <iostream>
using namespace std;

int main()
{
    // Defining variables
    string sFood = "pizza";
    string sName = "Bradley Summers";
    
    cout << "My favorite food is " << sFood << "." << endl; // Output favorite food
    cout << "My name is " << sName << "."; // Output name
    
    return 0;
}