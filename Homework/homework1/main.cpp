/*
* Name: Bradley Summers
* Student ID: 2564199
* Date: 3/2/15
* HW: homework1
* Problem: 
* I certify this is my own work and code
*/

#include <iostream>
using namespace std;

int main()
{
    int iNum1, iNum2, iSum, iProduct;
    
    cout << "Enter two integers. (Press return after entering a number)" << endl
         << "> ";
    cin >> iNum1;
    cout << "> ";
    cin >> iNum2;
    
    iSum        = iNum1 + iNum2;
    iProduct    = iNum1 * iNum2;
    
    cout << endl
         << "The sum of your numbers is        " << iSum << endl
         << "& the product of your numbers is  " << iProduct 
         << endl << endl;
    
    cout << "*********************************************" << endl
         << "*                                           *" << endl
         << "*         C C C           S S S       ! !   *" << endl
         << "*       C       C       S       S     ! !   *" << endl
         << "*     C               S               ! !   *" << endl
         << "*   C                   S             ! !   *" << endl
         << "*   C                     S S S       ! !   *" << endl
         << "*   C                           S     ! !   *" << endl
         << "*     C                           S   ! !   *" << endl
         << "*       C       C       S       S     ! !   *" << endl
         << "*         C C C           S S S       0 0   *" << endl
         << "*                                           *" << endl
         << "*   = = = = = = = = = = = = = = = = = = =   *" << endl
         << "*                                           *" << endl
         << "*     COMPUTER SCIENCE IS COOL STUFF !!     *" << endl
         << "*                                           *" << endl
         << "*********************************************" << endl;
    
    return 0;
}