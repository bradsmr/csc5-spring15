/*
* Name: Bradley Summers
* Student ID: 2564199
* Date: 3/9/15
* HW: homework2
* Problem: 
* I certify this is my own work and code
*/

#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    // 3) QUIZ AVERAGES
    
    // declare variables
    string sName1, sName2, sName3;
    double dQuiz1a, dQuiz1b, dQuiz1c,
            dQuiz2a, dQuiz2b, dQuiz2c,
            dQuiz3a, dQuiz3b, dQuiz3c,
            dQuiz4a, dQuiz4b, dQuiz4c,
            dQuiz1avg, dQuiz2avg, dQuiz3avg, dQuiz4avg;
    
    // get student names
    cout << "[Quiz Averages]" << endl << endl
         << "Enter the first name of three students: (separated by spaces & 12 characters max)" 
         << endl << "> ";
    cin >> sName1 >> sName2 >> sName3;
    
    // get quiz scores
    cout << "Enter " << sName1 << "'s scores for Quiz 1-4 separated by spaces."
         << endl << "> ";
    cin >> dQuiz1a >> dQuiz2a >> dQuiz3a >> dQuiz4a;
    
    cout << "Enter " << sName2 << "'s scores for Quiz 1-4 separated by spaces."
         << endl << "> ";
    cin >> dQuiz1b >> dQuiz2b >> dQuiz3b >> dQuiz4b;
    
    cout << "Enter " << sName3 << "'s scores for Quiz 1-4 separated by spaces."
         << endl << "> ";
    cin >> dQuiz1c >> dQuiz2c >> dQuiz3c >> dQuiz4c;
    
    // averages
    dQuiz1avg = (dQuiz1a + dQuiz1b + dQuiz1c) / 3;
    dQuiz2avg = (dQuiz2a + dQuiz2b + dQuiz2c) / 3;
    dQuiz3avg = (dQuiz3a + dQuiz3b + dQuiz3c) / 3;
    dQuiz4avg = (dQuiz4a + dQuiz4b + dQuiz4c) / 3;
    
    // output
    cout << endl << left;
    cout << setw(12) << "name"
         << setw(9) << "Quiz 1"
         << setw(9) << "Quiz 2"
         << setw(9) << "Quiz 3"
         << setw(9) << "Quiz 4" << endl;
    
    cout << setw(12) << "----"
         << setw(9) << "------"
         << setw(9) << "------"
         << setw(9) << "------"
         << setw(9) << "------" << endl;
    
    cout << setw(12) << sName1 << right
         << setw(4) << dQuiz1a
         << setw(9) << dQuiz2a
         << setw(9) << dQuiz3a
         << setw(9) << dQuiz4a << left << endl;
    
    cout << setw(12) << sName2 << right
         << setw(4) << dQuiz1b
         << setw(9) << dQuiz2b
         << setw(9) << dQuiz3b
         << setw(9) << dQuiz4b << left << endl;
    
    cout << setw(12) << sName3 << right
         << setw(4) << dQuiz1c
         << setw(9) << dQuiz2c
         << setw(9) << dQuiz3c
         << setw(9) << dQuiz4c << left << endl << endl;
    
    cout << fixed << setprecision(2); // only output 2 decimal places
    cout << setw(12) << "Average" << right
         << setw(4) << dQuiz1avg
         << setw(9) << dQuiz2avg
         << setw(9) << dQuiz3avg
         << setw(9) << dQuiz4avg << left << endl << endl;
    
    return 0;
}