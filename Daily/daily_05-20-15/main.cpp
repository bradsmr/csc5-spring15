#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>


using namespace std;

int     removeNegatives (int **p, int size);
void    bubbleSort      (int a[], int size);
void    fillArray       (int a[], int size, int min, int max); // <ctime> <cstdlib>
void    printArray      (int a[], int size, int format);

int main()
{
    srand(time(NULL));
    
    int arr_size = 10;
    int *nums = NULL;
    nums = new int[arr_size];    
    fillArray(nums, arr_size, -30, 30);
    
    cout << "> nums (initial)      ["; printArray(nums,arr_size,1); cout << "]\n";
    arr_size = removeNegatives(&nums,arr_size);
    cout << "> nums (negs removed) ["; printArray(nums,arr_size,1); cout << "]\n";
    bubbleSort(nums,arr_size);
    cout << "> nums (sorted)       ["; printArray(nums,arr_size,1); cout << "]\n";
    
    return 0;
}

int removeNegatives(int **p, int size)
{
    vector<int> v(size);        // create vector to store value locations
    int loc = 0;                // loc of (+) values (also counts temp array size)
    for(int i=0; i<size; i++) {
        if((*p)[i] > 0) {       // if (+), store location
            v[loc] = i;         
            loc++;              
        }
    }
    int *temp = NULL;           // new dynamic temp array
    temp = new int[loc];        // given size loc (# of positive values)
    
    for(int i=0; i<loc; i++)    // copy values at location to temp array
        temp[i] = (*p)[v[i]];              
    
    delete[] *p;                // deallocate original array
    
    *p = temp;
    size = loc;
    
    return size;
}

void bubbleSort(int a[], int size)      // bubble sort array
{
    int temp;
    bool complete = false;
    while(!complete)                    // while sort is not finished
    {
        complete = true;                // set true before checking to set false
        for(int i=0; i<size-1; i++) {   // loop through array
            if(a[i] > a[i+1]) {         // if second value is greater than first
                temp   = a[i];          // then .. swap values
                a[i]   = a[i+1];
                a[i+1] = temp;
                complete = false;       //      .. not done sorting, keep looping
            }
        }
    }
}

void fillArray(int a[], int size, int min, int max)
{
    for(int i=0; i<size; i++)
        a[i] = min + (rand() % (int)(max - min + 1));
}

void printArray(int a[], int size, int format)
{                                               /************************/
    switch(format){                             /* Format:              */
        case 0:     // no formatting            /* 0 = no formatting    */
            for(int i=0; i<size; i++)           /* 1 = spaces           */
                cout << a[i]; break;            /* 2 = spaces & commas  */
        case 1:     // spaces                   /* 3 = newlines         */
            for(int i=0; i<size; i++)           /************************/
            {
                if(i == size-1) {               // if last value ..
                    cout << a[i]; break;        // .. don't add space
                }
                cout << a[i] << ' ';
            } break;
        case 2:     // spaces & commas
            for(int i=0; i<size; i++)
            {
                if(i == size-1) {               // if last value ..
                    cout << a[i]; break;        // .. don't add comma
                }
                cout << a[i] << ", ";
            } break;
        case 3:     // newlines
            for(int i=0; i<size; i++)
                cout << a[i] << '\n'; break; 
        default:
            for(int i=0; i<size; i++)
                cout << a[i];
    }
}