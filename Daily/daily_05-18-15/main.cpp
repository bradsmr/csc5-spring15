#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

double getPositiveAvg(int a[], int size);
void   fillArray     (int a[], int size, int min, int max); // <time.h> <stdlib.h>
void   printArray    (int a[], int size, int format);

int main()
{
    srand(time(NULL));
    
    const int ARRAY_SIZE  = 10;         // initialize nums array
    int nums[ARRAY_SIZE];
    fillArray(nums, ARRAY_SIZE, 5, 28);
    cout << "Array:  ["; printArray(nums,ARRAY_SIZE,2); cout << "]\n";
    cout << "Average: " << getPositiveAvg(nums,10);
    return 0;
}

double getPositiveAvg(int a[], int size)
{
    if(size == 0) return 0;             // error check: no values, so avg is 0
    double paSum = 0, paAmt = 0;        // positive array sum & amount
    for(int i=0; i<size; i++)           // loop through array
    {         
        if(a[i] > 0)                    // if a[i] is positive ..
        {                  
            paSum += a[i];              // .. add to sum
            paAmt++;                    // .. & increase amount for avg calc
        }
    }
    return paSum / paAmt;               // return avg
}

void fillArray(int a[], int size, int min, int max)
{
    for(int i=0; i<size; i++)
        a[i] = min + (rand() % (int)(max - min + 1));
}

void printArray(int a[], int size, int format)
{                                               /************************/
    switch(format){                             /* Format:              */
        case 0:                                 /* 0 = no formatting    */
            for(int i=0; i<size; i++)           /* 1 = spaces           */
                cout << a[i]; break;            /* 2 = spaces & commas  */
        case 1:                                 /* 3 = newline          */
            for(int i=0; i<size; i++)           /************************/
            {
                if(i == size-1) {               // if last value ..
                    cout << a[i]; break;        // .. don't add space
                }
                cout << a[i] << ' ';
            } break;
        case 2:
            for(int i=0; i<size; i++)
            {
                if(i == size-1) {               // if last value ..
                    cout << a[i]; break;        // .. don't add comma
                }
                cout << a[i] << ", ";
            } break;
        case 3:
            for(int i=0; i<size; i++)
                cout << a[i] << '\n'; break; 
        default:
            for(int i=0; i<size; i++)
                cout << a[i];
    }
}