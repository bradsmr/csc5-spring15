#include <iostream>
using namespace std;

// 1. create temp array
// 2. sort temp array
// 3. median value is middle of sorted array
// 4. use findloc function to find loc in original array

void bubbleSort(int a[], int size);
int  getLoc(int a[], int size, int search);
int  getMedianLoc(int a[], int size);
void outpArray(int a[], int size);

int main()
{
    const int ARRAY_SIZE  = 10;           // initialize nums array
    int nums[ARRAY_SIZE] = { 1,3,5,7,9,
                             2,4,6,8,10 };
    
    for(int i=0; i<ARRAY_SIZE; i++)     // output array pre-sort
        cout << nums[i];
    
    bubbleSort(nums,ARRAY_SIZE);        // sort array
    
    cout << endl;                       // output array post-sort
    for(int i=0; i<ARRAY_SIZE; i++)
        cout << nums[i];
    cout << endl << endl;
    
    cout << "Loc of median of a[] = " <<  getMedianLoc(nums,ARRAY_SIZE);
    
    return 0;
}

void bubbleSort(int a[], int size)      // bubble sort array
{
    int temp;
    bool complete = false;
    while(!complete)                    // while sort is not finished
    {
        complete = true;    
        for(int i=0; i<size-1; i++) {   // loop through array
            if(a[i] > a[i+1]) {         // if second value is greater than first
                temp   = a[i];          // then .. swap values
                a[i]   = a[i+1];
                a[i+1] = temp;
                complete = false;       //      .. not done sorting
            }
        }
    }
}

int getLoc(int a[], int size, int search)
{
    for(int i=0; i<size; i++)           // loop through array
        if(a[i] == search) return i;    // if selection == search, return loc
    return -1;                          // else return error (value not found)
}

int getMedianLoc(int a[], int size)     // get location of median of array
{
    int temp[size];                     // create temp array
    for(int i=0; i<size; i++)           // copy to temp array
        temp[i] = a[i];
    bubbleSort(temp, size);             // sort temp array
    int median = temp[size/2];          // find median
    return getLoc(a, size, median);     // return median location
}