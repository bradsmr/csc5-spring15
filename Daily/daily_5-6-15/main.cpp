#include <iostream>
#include <vector>
#include <time.h>
#include <stdlib.h>

using namespace std;

int even_count_from_vector(const vector<int> &v);

int main()
{
    srand(time(NULL)); // initialize random seed    
    
    vector<int> nums(20); // declare nums vector (size 20))    
    bool exit = false;
    char exit_char;
    double run_count = 0, even_count;
    
    cout << "Enter [RETURN] to continue, [Q] to quit.";
    cin.get();
    while(!exit)
    {
        for(int i=0; i<nums.size(); i++) // assign rand value 1-100 to nums
        {
            nums[i] = rand() % 100 + 1;
            if(nums[i]%2 == 0)
                cout << '[' << nums[i] << "] "; // brackets around even numbers
            else
                cout << nums[i] << ' ';
        }

        cout << "\n\n" << even_count_from_vector(nums) 
             << " numbers in the vector 'nums' are even.\n\n";
        
        run_count++;
        even_count = even_count + even_count_from_vector(nums);
        
        cin.get(exit_char);
        if(exit_char == 'q') exit = true;
        
    }
    
    cout << "Out of [" << run_count*20 << "] numbers, "
         << "[" << even_count << "] were even.\n"
         << "That's " << (even_count/(run_count*20)) * 100 << "% even numbers.";
    return 0;
}

int even_count_from_vector(const vector<int> &v)
{
    int even_count = 0;
    for(int i=0; i<v.size(); i++)
    {
        if( v[i] % 2 == 0) // if vector value is even
            even_count++;           // add to even count (n)
    }
    return even_count;
}