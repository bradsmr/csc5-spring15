#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    // write to file
    ofstream outfile;
    outfile.open("test.txt");
    outfile << "Hello World";
    outfile.close();
    
    // read
    ifstream infile;
    infile.open("test.txt");
    string input;
    infile >> input;
    infile.close();
    
    for( int i=0; i<9999999999; i++)
    {
        for (int j=0; j<9999999999; j++)
        {
            cout << "*";
        }
        cout << endl;
    }
    
    return 0;
}