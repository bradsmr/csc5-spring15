// bubble sort

#include <iostream>
#include <iomanip>
#include <vector>
#include <time.h>
#include <windows.h>

using namespace std;

void bubbleSort(vector<int> &v);

int main()
{
    srand(time(NULL));
    
    int n;
    vector<int> nums(100);
    for(int i=0; i<nums.size(); i++) //rand value between 1 and nums.size() to nums
    {
        nums[i] = rand() % 999 + 1;
    }
    
    cout << right << "UNSORTED: \n\n";
    n = 1;                              // initialize "line counter"
    for(int i=0; i<nums.size(); i++)    // loop through vector
    {                                   
        Sleep(1);                      // wait 1 millisecond
        cout << setw(4) << nums[i];     // write current vector value
        if(n%20 == 0){                  // if 20 values are on current line
            cout << '\n';                       // then go to new line
            n = 0;                              // reset "line counter"
        }
        n++;                            // add to "line counter"
        
        cout.flush();
    }
    cout << '\n';
    
    bubbleSort(nums);
   
    cout << "\n\nSORTED: \n\n";
    n = 1;                              // initialize "line counter"
    for(int i=0; i<nums.size(); i++)    // loop through vector
    {                                   
        Sleep(1);                      // wait 1 millisecond
        cout << setw(4) << nums[i];     // write current vector value
        if(n%20 == 0){                  // if 20 values are on current line
            cout << '\n';                       // then go to new line
            n = 0;                              // reset "line counter"
        }
        n++;                            // add to "line counter"
        
        cout.flush();
    }
    cout << '\n';
    
    cin.get();
    
}

void bubbleSort(vector<int> &v)
{
    bool complete = false;
    while(!complete) {      // while sort is not finished
        complete = true;    
        for(int i=0; i<v.size()-1; i++) { // loop through vector
            if(v[i] > v[i+1]) {         // if second value is greater than first
                swap(v[i],v[i+1]);      // swap values
                complete = false;       // not done sorting
            }
        }
    }
}