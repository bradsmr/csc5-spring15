// in class practice 3-9-15

#include <iostream>
using namespace std;

int main()
{
    // switch
    cout << "Please enter a number: " << endl;
    cout << "1 for Apples" << endl
         << "2 for Oranges" << endl
         << "3 for Grapes" << endl
         << "> ";
    
    int userInput;
    cin >> userInput;
    
    switch(userInput)
    {
        case 1:
            cout << "Apples were selected";
            break;
        case 2:
            cout << "Oranges were selected";
            break;
        case 3:
            cout << "Grapes were selected";
            break;
        default:
            cout << "No fruit was selected";
    }
    
    // string member finctions
    
    cout << endl << "Please enter a string: ";
    string stringinput;
    cin >> stringinput;
    
    cout << "The size of the string is: " << stringinput.size() << endl;
    return 0;
}