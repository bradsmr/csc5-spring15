#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
    srand(time(0));
    
    // 1) GRADES
    
    // 2) BABYLONIAN ALGORITHM
    
    // 3) LEAP YEAR
    
    int iYear;
    cout << "Enter a year" << endl
         << ">";
    cin >> iYear;
    
    if(iYear%4 == 0)
    {
        if     (iYear%400 == 0)
            cout << "Leap year." << endl << endl;
        else if(iYear%100 != 0)
            cout << "Leap year." << endl << endl;
        else
            cout << "Not a leap year." << endl << endl;
    }
    else
        cout << "Not a leap year." << endl << endl;
    
    // 4) LEAP YEAR (COMPOUNDED IF STATEMENT)
    
    int iYear2;
    cout << "Enter a year" << endl
         << ">";
    cin >> iYear2;
    if(iYear2%4 == 0 && (iYear2%400 == 0 || iYear2%100 != 0))
        cout << "Leap year." << endl << endl;
    else
        cout << "Not a leap year." << endl << endl;
    
    // 5) GUESSING GAME
    
    int iRand;
    int iLives = 10;
    int iGuess;
    bool bVictory = false;
    
    // entire game loop
    while(bVictory == false)
    {
        cout << "Guess the number between 1 and 100!" << endl;;
        iRand = rand() % 100; // seed random number

        // main play loop
        while(iLives>0 && bVictory == false)
        {
            cout << "Lives: " << iLives << endl;
            cout << "Guess (1-100)" << endl
                 << "> ";
            cin >> iGuess;
            if(iGuess == iRand)
            {
                cout << "Congratulations! You guessed the random number " << iRand;
                bVictory = true;
            }
            else if(iGuess < iRand)
                cout << "Too low." << endl << endl;
            else if(iGuess > iRand)
                cout << "Too high." << endl << endl;
            iLives--;
        }
        
        // if player loses, ask to play again
        if(bVictory == false && iLives <= 0) 
        {
            cout << "Game over. Would you like to play again? (y/n)" << endl
                 << "> ";
            char cPlayAgain;
            cin >> cPlayAgain;
            if(cPlayAgain == 'y')
                iLives = 10;
            else if(cPlayAgain == 'n')
                bVictory = true;
            else
                cout << "Invalid entry.";
        }
        
        cout << endl << endl;
    }
    return 0;
}