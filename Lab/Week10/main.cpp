#include <iostream>
using namespace std;

void problem1(); // Prototype functions
void problem2();
void problem3();
void problem4();
void problem5();

int main()
{
    bool exit = false;
    while(!exit)
    {
        cout << "Which problem? (1-5) (q to exit)\n> ";
        char   menu;
        cin >> menu;
        switch(menu)
        {
            case '1': problem1(); break;
            case '2': problem2(); break;
            case '3': problem3(); break;
            case '4': problem4(); break;
            case '5': problem5(); break;
            case 'q': exit=true;  break;
            default:  cout << "\nInvalid entry.\n\n";
        }
    }
    return 0;
}

void problem1() // 1) READ FILE & STORE VALUES TO VECTOR
{
    
}

void problem2() // 2) STORE RANDOM VALUES BETWEEN 50-100 (VECTOR SIZE 50)
{
    
}

void problem3() // 3) FUNCTION TO CHECK IF VALUE IS IN VECTOR
{
    
}

void problem4() // 4) PROGRAM 3 + CHECKS FOR DUPLICATES
{
    
}

void problem5() // 5) PROGRAM 4 + REMOVES DUPLICATES
{
    
}

