#include <iostream>
using namespace std;

int main()
{
    string sUserName; // Define string variable
    
    cout << "Hello, my name is Hal!" << endl;
    cout << "What is your name?" << endl << "> "; // Prompt user input
    cin >> sUserName; // Get user input
    cout << "Hello, " << sUserName << ". I am glad to meet you."; // Output user's name with message
    
    return 0;
}
