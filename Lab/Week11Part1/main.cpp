#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>

using namespace std;

void problem1();
void problem2();
void problem3();
void problem4();
void problem5();
void problem6();
void problem7();
void problem8();

int main()
{
    bool exit = false;
    while(!exit)
    {
        cout << "Which problem? (1-8) (q to exit)\n> ";
        char   menu;
        cin >> menu;
        switch(menu)
        {
            case '1': problem1(); break;
            case '2': problem2(); break;
            case '3': problem3(); break;
            case '4': problem4(); break;
            case '5': problem5(); break;
            case '6': problem6(); break;
            case '7': problem7(); break;
            case '8': problem8(); break;
            case 'q': exit=true;  break;
            default:  cout << "Invalid entry.\n\n";
        }
    }
    return 0;
}

void problem1() // VECTOR & ARRAY W/ 10 RAND NUMBERS
{
    cout << "\nProblem 1:\n\n";
    
    srand(time(NULL));
    const int SIZE = 10;
    vector<int> nums_v(SIZE);
    int nums_a[SIZE];
    
    for(int i=0; i<SIZE; i++)
    {
        nums_v[i] = rand() % 1000 + 1;
        nums_a[i] = rand() % 1000 + 1;
    }
    for (int i=0; i<SIZE; i++)
    {
        cout << "";
    }
    
}

void problem2()
{
    
}

void problem3()
{
    
}

void problem4()
{
    
}

void problem5()
{
    
}

void problem6()
{
    
}

void problem7()
{
    
}

void problem8()
{
    
}