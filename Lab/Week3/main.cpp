#include <iostream>
#include <iomanip> // allows output manipulation
using namespace std;

int main()
{
    /*
    // 1) STRING CONCATENATION
    
    string varA1 = "1";
    int varA2 = 2;
    
    cout << varA1 << "+" << varA1 << "=" << varA1+varA1 << endl;
    cout << varA2 << "+" << varA2 << "=" << varA2+varA2 << endl << endl;
    
    // 2) SLUGGING PERCENTAGE
    
    int   iSingles, iDoubles, iTriples, iHomeRuns, iAtBats;
    float fSingles, fDoubles, fTriples, fHomeRuns, fAtBats, fSluggingPercentage;
    
    // get stat inputs as integers
    cout << "Enter a number of singles." << endl
         << "> ";
    cin >> iSingles;
    cout << "Enter a number of doubles." << endl
         << "> ";
    cin >> iDoubles;
    cout << "Enter a number of triples." << endl
         << "> ";
    cin >> iTriples;
    cout << "Enter a number of home runs." << endl
         << "> ";
    cin >> iHomeRuns;
    cout << "Enter a number of at-bats." << endl
         << "> ";
    cin >> iAtBats;
    
    // convert integer inputs to floats
    fSingles  = static_cast<float>(iSingles);
    fDoubles  = static_cast<float>(iDoubles);
    fTriples  = static_cast<float>(iTriples);
    fHomeRuns = static_cast<float>(iHomeRuns);
    fAtBats   = static_cast<float>(iAtBats);
    
    // calculate slugging percentage using floats
    fSluggingPercentage = (fSingles + (fDoubles*2) + (fTriples*3) + (fHomeRuns*4)) / fAtBats;
    cout << endl 
         << "Your slugging percentage is: " << fSluggingPercentage << endl << endl;
    
    // 3) VARIABLE SWITCH / OUTPUT MANIPULATION
    
    int iInput1, iInput2, iTemp1;
    
    // get user input
    cout << "Enter two numbers between 1 and 1000 (separated by a return)." << endl
         << "> ";
    cin >> iInput1;
    cout << "> ";
    cin >> iInput2;
    
    // output initial values of inputs in tables
    cout << endl << left; // align output to the left
    cout << "X = " << setw(4) << iInput1 << " ";
    cout << "Y = " << setw(4) << iInput2 << endl;
    
    // store value of iInput1 in temp variable and then switch values
    iTemp1  = iInput1;
    iInput1 = iInput2;
    iInput2 = iTemp1;
    
    // output dashes & switched values
    cout << setfill('-') << setw(79) << "-" << endl << setfill(' '); // output dashes for 80 characters & clear setfill
    cout << "X = " << setw(4) << iInput1 << " ";
    cout << "Y = " << setw(4) << iInput2 << endl << endl;
    
    // 4) IF STATEMENTS OUTPUT?
    
    int num1 = 4;
    int num2 = 0;
    
    if (num1 == 4) {
        num2 = 4;
    }
    else if (num1 == 9) {
        num2 = 5;
    }
    else {
        num2 = 6;
    }
    
    // output values?
    cout << "num1: " << num1 << endl;
    cout << "num2: " << num2 << endl << endl;
   */ 
    // 5) MONEY & CHANGE PROGRAM
    
    double dPayment;
    double dAmountDue = 18.02;
    double dChange, dChangeTotal;
    int iChangeInt;
    //const int I_MULT = 100;
    
    // prompt user for input
    cout << "You owe $" << dAmountDue << ". Please input your payment." << endl
         << "> $";
    cin >> dPayment;
    cout << endl;   
    
    if (dPayment > dAmountDue) // if payment satisfies the amount due, & need to give change:
    {
        cout << "Thank you for your payment of $" << dPayment << "." << endl;
        
        // calculate change    
        dChange = dPayment - dAmountDue;    
        dChangeTotal = dChange;
        iChangeInt = static_cast<int>(dChange*100);
        
        int iTwentyDollars  = iChangeInt / 2000;
        iChangeInt -= iTwentyDollars*2000;
        
        int iTenDollars  = iChangeInt / 1000;
        iChangeInt -= iTenDollars*1000;
        
        int iFiveDollars  = iChangeInt / 500;
        iChangeInt -= iFiveDollars*500;
        
        int iDollars  = iChangeInt / 100;
        iChangeInt -= iDollars*100;
        
        int iQuarters = iChangeInt / 25;
        iChangeInt -= iQuarters*25;
        
        int iDimes    = iChangeInt / 10;
        iChangeInt -= iDimes*10;
        
        int iNickels  = iChangeInt / 5;
        iChangeInt -= iNickels*5;
        
        int iPennies  = iChangeInt / 1;
        iChangeInt -= iPennies;
        
        // output change
        cout << "Your change is "
             << iTwentyDollars << " $20 dollar bills, "
             << iTenDollars    << " $10 dollar bills, "
             << iFiveDollars   << " $5 dollar bills, "
             << iDollars       << " $1 dollar dollars, "
             << iQuarters      << " quarters, "
             << iDimes         << " dimes, "
             << iNickels       << " nickels, and "
             << iPennies       << " pennies, for a total of $" << dChangeTotal << "." << endl << endl;
        
        cout << "Your change is ";
        
        if(iTwentyDollars > 0)
            cout << iTwentyDollars << " $20 dollar bills, ";
        if(iTenDollars > 0)
            cout << iTenDollars << " $10 dollar bills, ";
        if(iFiveDollars > 0)
            cout << iFiveDollars << " $5 dollar bills, ";
        if(iDollars > 0)
            cout << iDollars << " $1 dollar bills, ";
        if(iQuarters > 0)
            cout << iQuarters << " quarters, ";
        if(iDimes > 0)
            cout << iDimes << " dimes, ";
        if(iNickels > 0)
            cout << iNickels << " nickels, ";
        if(iPennies > 0)
            cout << iPennies << " pennies, ";
        
        cout << "for a total of $" << dChangeTotal << "." << endl << endl;
        
        
    }
    else if (dPayment == dAmountDue) // if payment satisfies amount due & no change is due:
    {
        cout << "Thank you for your payment of $" << dPayment << ". You are due no change." << endl;
    }
    else if (dPayment < dAmountDue) // if payment is not enough to satisfy amount due:
    {
        cout << "I'm sorry, but your payment of $" << dPayment << " is insufficient." << endl;
    }
    
    return 0;
}