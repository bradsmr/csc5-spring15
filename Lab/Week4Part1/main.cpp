#include <iostream>

using namespace std;

int main()
{
    // 1) GRADE LETTER FROM VALUE
    
    int  iGradeValue;
    char cGradeLetter;
    int  iLoops = 0;
    
    for(int i=0; i<10; i++)
    {
        cout << "Input a grade value integer. (1-100)" << endl
             << "> ";
        cin >> iGradeValue;

        if     (iGradeValue >= 90 && iGradeValue <= 100)
            cGradeLetter = 'A';
        else if(iGradeValue >= 80 && iGradeValue <= 89)
            cGradeLetter = 'B';
        else if(iGradeValue >= 70 && iGradeValue <= 79)
            cGradeLetter = 'C';
        else if(iGradeValue >= 60 && iGradeValue <= 69)
            cGradeLetter = 'D';
        else if(iGradeValue >= 0  && iGradeValue <= 59)
            cGradeLetter = 'F';

        if(cGradeLetter == 'B' || cGradeLetter == 'C' || cGradeLetter == 'D')
            cout << "Your grade is a " << cGradeLetter << ".";
        else if(cGradeLetter == 'A' || cGradeLetter == 'F')
            cout << "Your grade is an " << cGradeLetter << ".";
        else
            cout << "Invalid entry, try again." << endl << endl;
        
        cout << endl << endl;
    }
    
    // 4) GRADE VALUE FROM LETTER
    
    string sGradeInput;
    cout << "Input a grade letter between 'A+' and 'B-" << endl
         << "> ";
    cin >> sGradeInput;
    
    cout << "Grade value: ";
    if     (sGradeInput == "A+")
        cout << "100+";
    else if(sGradeInput == "A")
        cout << "93 - 100";
    else if(sGradeInput == "A-")
        cout << "90 - 92.9";
    else if(sGradeInput == "B+")
        cout << "87 - 89.9";
    else if(sGradeInput == "B")
        cout << "83 - 86.9";
    else if(sGradeInput == "B-")
        cout << "80 - 82.9";
    else
        cout << "Invalid entry.";   
    
    cout << endl << endl;
    return 0;
}