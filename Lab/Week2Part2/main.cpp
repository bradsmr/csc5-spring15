#include <iostream>

using namespace std;

int main()
{
    
    // 1) HAL INPUT/OUTPUT
    
    string sUserName; // Define string variable
    
    cout << "Hello, my name is Hal!" << endl;
    cout << "What is your name?" << endl << "> "; // Prompt user input
    cin >> sUserName; // Get user input
    cout << "Hello, " << sUserName << ". I am glad to meet you." // Output user's name with message
         << endl << endl; 
    
    
    // 2) SWITCH A & B VALUES
    
    int iA = 20;
    int iB = 1000;
    cout << "a: " << iA << "  " << "b: " << iB << endl;
    iA = iA+iB; // 5+10  = 15
    iB = iA-iB; // 15-10 = 5
    iA = iA-iB; // 15-5  = 10
    cout << "a: " << iA << " " << "b: " << iB << endl << endl;
    
    
    
    // 3) METERS TO MILES, FEET, & INCHES CONVERSION
    
    double dInput;
    double dMiles;
    double dFeet;
    double dInches;
    
    const double M_TO_MI = 1609.344;
    const double M_TO_FT = 3.281;
    
    cout << "Enter an amount in meters." << endl
         << "> ";
    cin >> dInput;
    
    dMiles  = dInput/M_TO_MI;
    dFeet   = dInput*M_TO_FT;
    dInches = (dInput*M_TO_FT)*12;
    
    cout << endl
         << "Miles:  " << dMiles << endl
         << "Feet:   " << dFeet << endl
         << "Inches: " << dInches << endl << endl;
    
    
    
    // 4) INCREMEMNT BY 5
 
    int iC;
    
    cout << "Enter an integer." << endl
         << "> ";
    cin >> iC;
    
    cout << iC << " + 5 = ";
    iC = iC + 5;
    cout << iC << endl << endl;
    
    
    
    // 5) TEST SCORES AVERAGE
    
    double iTestScore1, iTestScore2, iTestScore3;
    double iTestScoreAverage;
    
    cout << "Enter three test scores. (Separated by a single space each)" << endl
         << "> ";
    cin >> iTestScore1 >> iTestScore2 >> iTestScore3;
    
    iTestScoreAverage = ( iTestScore1 + iTestScore2 + iTestScore3 )/3;
    cout << endl << "Your test score average is: " << iTestScoreAverage << endl << endl;
    
    
    
    // 6) SPLIT NUMBER INTO DIGITS
    
    int iInput;
    
    cout << "Enter an integer." << endl
         << "> ";
    cin >> iInput;
    
    cout << "..." << endl;
    
         // ... stuck
    
    return 0;
}
