// project2_tictactoe
// Bradley Summers

#include <iostream>
#include <cstdlib> // rand() & time
using namespace std;

// function prototypes
void pvp();
void pvai1();
void pvai2();
void aivai();

void draw_board();
bool endgame();
void p_turn(int gm);
void ai_turn();

int  ai1_move();
int  ai2_move();


// global variables
char turn;
bool draw = false;
char board[3][3] = {'1','2','3','4','5','6','7','8','9'};

// main menu loop
int main()
{
    srand(time(NULL)); // initialize random seed
    
    int menu;
    bool menuloop = true;
    
    cout << "Welcome to TIC-TAC-TOE!" << endl;
            
    // menu loop
    while(menuloop == true) {
        menuloop = false;
        
        // display menu
        cout << "Select a menu option."       << endl
             << "[1] Player  vs Player"     << endl
             << "[2] Player  vs Perfect AI" << endl
             << "[3] Player  vs Dumb AI"    << endl
             << "[4] Dumb AI vs Dumb AI"    << endl
             << "[5] View Stats"            << endl
             << "> ";

        cin  >> menu;
        
        while(cin.fail()) // continue prompting user for input while invalid
        {
            cout << "Invalid Entry! Enter a number (1-4)\n> ";
            cin.clear();
            cin.ignore(256,'\n');
            cin >> menu;
        }
        
        switch(menu)
        {
            case 1: pvp(); break;
            case 2: pvai1(); break;
            case 3: pvai2(); break;
            case 4: aivai(); break;
            default: // error check: check if input is 1-4
                cout << "Invalid Entry! Enter a number (1-4)\n\n";
                menuloop = true; // if invalid, run menu loop again
        }        
    }
    
    return 0;
}


// ### GAME INIT: ###

// player vs player
void pvp()
{
    cout << "\nPlayer vs Player!\n"
         << "Player 1 is [X] & Player 2 is [O]\n\n";
    
    turn = 'X'; // player 1 is the first to move
    
    while (!endgame()) // main game loop while checking for endgame conditions
    {
        draw_board();
        p_turn(0);
        endgame();
    }
    
    // after endgame is met, check who won
    if (turn == 'O' && !draw) // if X made last move & not a draw, X wins
    {
        draw_board();
        cout << "\n\nPlayer 1 [X] is victorious!\n";
    }
    else if (turn == 'X' && !draw) // if O made last move & not a draw, O wins
    {
        draw_board();
        cout << "\n\nPlayer 2 [O] is victorious!\n";
    }
    else
    {
        draw_board();
        cout << endl << endl << "It's a draw! Game Over!\n";
    }
}

// player vs perfect ai
void pvai1() 
{
    cout << "\nPlayer vs Perfect AI!\n"
         << "Player 1 is [X] & Perfect AI is [O]\n\n";
    
    turn = 'X'; // player 1 is the first to move
    while (!endgame()) // main game loop while checking for endgame conditions
    {
        draw_board();
        p_turn(1);
        endgame();
    }
    
    // after endgame is met, check who won
    if (turn == 'O' && !draw) // if X made last move & not a draw, X wins
    {
        draw_board();
        cout << "\n\nPlayer 1 [X] is victorious!\n";
    }
    else if (turn == 'X' && !draw) // if O made last move & not a draw, O wins
    {
        draw_board();
        cout << "\n\nPerfect AI [O] is victorious!\n";
    }
    else
    {
        draw_board();
        cout << endl << endl << "It's a draw! Game Over!\n";
    }    
}

// player vs dumb ai
void pvai2()
{
    cout << "\nPlayer vs Dumb AI!\n"
         << "Player 1 is [X] & Dumb AI is [O]\n\n";
    
    turn = 'X'; // player 1 is the first to move
    while (!endgame()) // main game loop while checking for endgame conditions
    {
        draw_board();
        p_turn(2);
        endgame();
    }
    
    // after endgame is met, check who won
    if (turn == 'O' && !draw) // if X made last move & not a draw, X wins
    {
        draw_board();
        cout << "\n\nPlayer 1 [X] is victorious!\n";
    }
    else if (turn == 'X' && !draw) // if O made last move & not a draw, O wins
    {
        draw_board();
        cout << "\n\nDumb AI [O] is victorious!\n";
    }
    else
    {
        draw_board();
        cout << endl << endl << "It's a draw! Game Over!\n";
    }
}

// dumb ai vs dumb ai
void aivai()
{
    cout << "\nDumb AI [1]vs Dumb AI [2]!\n"
         << "Dumb AI [1] is [X] & Dumb AI [2] is [O]\n\n";
    
    turn = 'X'; // dumb ai [1] is the first to move
    while (!endgame()) // main game loop while checking for endgame conditions
    {
        draw_board();
        ai_turn();
        endgame();
    }
    
    // after endgame is met, check who won
    if (turn == 'O' && !draw) // if X made last move & not a draw, X wins
    {
        draw_board();
        cout << "\n\nDumb AI [1] [X] is victorious!\n";
    }
    else if (turn == 'X' && !draw) // if O made last move & not a draw, O wins
    {
        draw_board();
        cout << "\n\nDumb AI [2] [O] is victorious!\n";
    }
    else
    {
        draw_board();
        cout << endl << endl << "It's a draw! Game Over!\n";
    }
}

// ### END GAME INIT ###


// draw game board
void draw_board()
{
    cout << board[0][0] << '|' << board[0][1] << '|' << board[0][2] << endl;
    cout << "-+-+-" << endl;
    cout << board[1][0] << '|' << board[1][1] << '|' << board[1][2] << endl;
    cout << "-+-+-" << endl;
    cout << board[2][0] << '|' << board[2][1] << '|' << board[2][2] << endl;
}

// end game check
bool endgame()
{
    // check for victory
    for (int i=0; i<3; i++) 
    {
        // check for 3 in a row for every board position using for loop
        if ((board[i][0] == board[i][1] && board[i][1] == board[i][2])
         || (board[0][i] == board[1][i] && board[1][i] == board[2][i])
         || (board[0][0] == board[1][1] && board[1][1] == board[2][2])
         || (board[0][2] == board[1][1] && board[1][1] == board[2][0])) 
            return true; // end the game
    }
    
    // check for draw
    for (int i=0; i<3; i++) 
    {
        for (int j=0; j<3; j++)
        {
            if (board[i][j] != 'X' && board[i][j] != 'O')
                return false; // end the game
        }
    }
    draw = true;
    return true;
}


// ### TURNS: ###

// player turn
void p_turn(int gm)
{
    int move;
    int board_r = 0, board_c = 0;
    
    if (turn == 'X')
    {
        cout << "Player 1's turn! [X] > ";
        cin >> move;
        while(cin.fail()) // continue prompting user for input while invalid
        {
            cout << "Invalid Entry! Enter a number (1-9)\n> ";
            cin.clear();
            cin.ignore(256,'\n');
            cin >> move;
        }
        cout << endl;
    }
    
    else if (turn == 'O')
    {
        switch(gm)
        {
            case 0: // PVP -> get player 2 input
                cout << "Player 2's turn! [O] > ";
                // get player input & error check #1: check if input is 'int'
                cin >> move;
                while(cin.fail()) // continue prompting user for input while invalid
                {
                    cout << "Invalid Entry! Enter a number (1-9)\n> ";
                    cin.clear();
                    cin.ignore(256,'\n');
                    cin >> move;
                }
                cout << endl;
                break;
            case 1: // pvai1 -> get perfect ai move
                move = ai1_move();
                break;
            case 2: // pvai2 -> get dumb ai move
                move = ai2_move();
            default:
                // default stuff
                break;
        }            
    }
    
    // define rows and columns for board positions based on input
    switch (move)
    {
        case 1: board_r = 0; board_c = 0; break;
        case 2: board_r = 0; board_c = 1; break;
        case 3: board_r = 0; board_c = 2; break;
        
        case 4: board_r = 1; board_c = 0; break;
        case 5: board_r = 1; board_c = 1; break;
        case 6: board_r = 1; board_c = 2; break;
        
        case 7: board_r = 2; board_c = 0; break;
        case 8: board_r = 2; board_c = 1; break;
        case 9: board_r = 2; board_c = 2; break;
        
        default: // error check #2: check if input is 1-9
            cout << "Invalid Entry! Enter a number (1-9)" << endl;
            p_turn(gm); // if invalid, run turn again
    }
    
    // check if board space is occupied & record player move
    // X turn
    if (turn == 'X' && board[board_r][board_c] != 'X' && board[board_r][board_c] != 'O')
    {
        board[board_r][board_c] = 'X';
        turn = 'O';
    }
    // O turn
    else if (turn == 'O' && board[board_r][board_c] != 'X' && board[board_r][board_c] != 'O')
    {
        board[board_r][board_c] = 'O';
        turn = 'X';
    }
    // error check #3: check if space is occupied
    else
    {
        cout << "Invalid Entry! Choose an unoccupied board space.\n";
        p_turn(gm); // if invalid, run turn again
    }

}

// ai turn (for ai vs ai)
void ai_turn()
{
    int board_r = 0, board_c = 0;
    int move = ai2_move();
    switch (move)
    {
        case 1: board_r = 0; board_c = 0; break;
        case 2: board_r = 0; board_c = 1; break;
        case 3: board_r = 0; board_c = 2; break;
        
        case 4: board_r = 1; board_c = 0; break;
        case 5: board_r = 1; board_c = 1; break;
        case 6: board_r = 1; board_c = 2; break;
        
        case 7: board_r = 2; board_c = 0; break;
        case 8: board_r = 2; board_c = 1; break;
        case 9: board_r = 2; board_c = 2; break;
    }
    // X turn
    if(turn == 'X')
    {
        board[board_r][board_c] = 'X';
        turn = 'O';
        cout << "\nDumb AI [1] played " << move << ", Dumb AI [2]'s turn!\n\n";
    }
    // O turn
    else if(turn == 'O')
    {
        board[board_r][board_c] = 'O';
        turn = 'X';
        cout << "\nDumb AI [2] played " << move << ", Dumb AI [1]'s turn!\n\n";
    }
}


// ### AI MOVES: ###

// perfect ai move [incomplete]
int  ai1_move()
{
    
}

// dumb ai move
int  ai2_move()
{
    bool occupied = true;
    int board_r = 0;
    int board_c = 0;
    int move;
    while(occupied)
    {
        occupied = false;
        move = rand() % 9 + 1;
        switch (move) // assign board rows & columns based on input
        {
            case 1: board_r = 0; board_c = 0; break;
            case 2: board_r = 0; board_c = 1; break;
            case 3: board_r = 0; board_c = 2; break;

            case 4: board_r = 1; board_c = 0; break;
            case 5: board_r = 1; board_c = 1; break;
            case 6: board_r = 1; board_c = 2; break;

            case 7: board_r = 2; board_c = 0; break;
            case 8: board_r = 2; board_c = 1; break;
            case 9: board_r = 2; board_c = 2; break;
        }
        
        // if random choice is unoccupied, make that move
        if(board[board_r][board_c] != 'X' && board[board_r][board_c] != 'O')
            return move;
        // else, try again
        else
            occupied = true;
    }
}