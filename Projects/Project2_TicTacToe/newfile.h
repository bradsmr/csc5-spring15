#ifndef AI_H
#define AI_H

#include "game.h"


/*	This is an introduction to AI programming -- And we start right off the bat with a lesson

	LESSON #1:  There is no wrong way to program AI.

	Now that's not to say that your AI can't suck -- It's also not to say that your AI can't 
	be too easy or too hard.  All that means is there no "industry standard" or "way 
	that everybody does it" when dealing with AI.  There are common algorithms that are
	used, path finding algorithms for instance, but how they are used in each game is 
	usually quite different.

	For a game to have good AI, the most critical thing that needs to be established is 
	a crystal clear picture of how the AI should function in any given situation in the
	game.  We said we wanted the AI to be tough, but not impossible.  So the action plan
	for the AI I came up with goes like this:

	If the AI can move and win the game, do so
	Otherwise if the AI can move and prevent the player from winning, do so
	Otherwise if the AI can move to the center spot, do so
	Otherwise if the AI can move to a corner spot, do so
	Else just move to an open spot.

	That's our AI, simple but effective
*/

// This "moves" the AI
void AIMove(GAMEDATA &gamedata);

// If the AI can move to a spot and win the game, this moves the AI and returns true
// otherwise returns false
bool Move2Win(GAMEDATA &gamedata); 

// If the AI can move to a spot and block the player from winning on his next
// turn, this moves the AI to that spot and returns true, otherwise returns false
bool Move2Block(GAMEDATA &gamedata);  

// If the AI can move to the center OR to a corner spot (checked in that order), this 
// moves him to the open spot and returns true, otherwise returns false
bool Move2CenterOrCorner(GAMEDATA &gamedata);

// Moves the AI to the first open spot, starting from the top left corner
void Move2OpenSq(GAMEDATA &gamedata);

#endif